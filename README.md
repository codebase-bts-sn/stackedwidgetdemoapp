# StackedWidgetDemoApp

Exemple d'application Qt qui illustre l'utilisation de *widgets* empilables.

![alt text](img/screenshot.png "Qt App screenshot")

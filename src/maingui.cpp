#include <QRandomGenerator>

#include "maingui.h"
#include "ui_maingui.h"

MainGUI::MainGUI(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainGUI)
{
    ui->setupUi(this);

    connect(ui->rbtn0, &QRadioButton::clicked, this, &MainGUI::onRadioButtonClicked);
    connect(ui->rbtn25, &QRadioButton::clicked, this, &MainGUI::onRadioButtonClicked);
    connect(ui->rbtn50, &QRadioButton::clicked, this, &MainGUI::onRadioButtonClicked);
    connect(ui->rbtn75, &QRadioButton::clicked, this, &MainGUI::onRadioButtonClicked);
    connect(ui->rbtn100, &QRadioButton::clicked, this, &MainGUI::onRadioButtonClicked);

    connect(&_timer, &QTimer::timeout, this, &MainGUI::onTimerTimeout);

    ui->stackedWidget->setCurrentIndex(0);

    ui->rbtnFixed->click();
    ui->rbtn0->click();
}

MainGUI::~MainGUI()
{
    delete ui;
}

void MainGUI::onTimerTimeout()
{
    int val = QRandomGenerator::global()->bounded(0, 100);

    ui->progressBar->setValue(val);
    ui->lcd->display(QString::number(val));
}

void MainGUI::onRadioButtonClicked()
{
    QObject * sigSrc = sender();
    int val;

    if(sigSrc == ui->rbtn0) {
        val = 0;
    } else if(sigSrc == ui->rbtn25) {
        val = 25;
    } else if(sigSrc == ui->rbtn50) {
        val = 50;
    } else if(sigSrc == ui->rbtn75) {
        val = 75;
    } else if(sigSrc == ui->rbtn100) {
        val = 100;
    };
    ui->progressBar->setValue(val);
}


void MainGUI::on_rbtnRandom_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);

    _timer.setInterval(500);
    _timer.start();
}


void MainGUI::on_rbtnFixed_clicked()
{
    _timer.stop();

    ui->stackedWidget->setCurrentIndex(0);

    ui->rbtn0->setChecked(true);
    ui->progressBar->setValue(0);

}


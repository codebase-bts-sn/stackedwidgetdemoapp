#ifndef MAINGUI_H
#define MAINGUI_H

#include <QDialog>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainGUI; }
QT_END_NAMESPACE

class MainGUI : public QDialog
{
    Q_OBJECT

public:
    MainGUI(QWidget *parent = nullptr);
    ~MainGUI();

private:
    Ui::MainGUI *ui;
    QTimer _timer;

private slots:
    void onTimerTimeout();
    void onRadioButtonClicked();
    void on_rbtnRandom_clicked();
    void on_rbtnFixed_clicked();
};
#endif // MAINGUI_H
